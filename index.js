let posts = []; //mockdatabase

//count variable will start at 1
let count = 1;

//Add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	//prevents page from loading
	e.preventDefault();

	posts.push({
		id:count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value,
	})

	console.log(count);
	//Increment the id value in count variable
	count++;

	showPosts(posts); //invoke func
	alert('Successfully added.')
});

const showPosts = (posts) => {
	let postEntries = " "; //where html will be hold when we display post, sets html structure yo display new posts

	//post function will handle individual post
		posts.forEach((post) => {
			//continue to display html input when posting
			postEntries += `
				<div id="post-${post.id}">
					<h3 id="post-title-${post.id}">${post.title}</h3>
					<p id="post-body-${post.id}">${post.body}</p>
					<button onclick="editPost('${post.id}')">Edit</button>
					<button onclick="deletePost('${post.id}')">Delete</button>
				</div>
			`;
		})

		document.querySelector('#div-post-entries').innerHTML = postEntries;
}

//Edit post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}

//update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	for(let i = 0; i < posts.length; i++){
		//index number of posts
		//tostring for compatibility of data type
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			document.querySelector('#txt-edit-title').value = " ";
			document.querySelector('#txt-edit-body').value = " ";
			alert('Successfully updated!');

			break;
		}
	}
})

//delete post


const deletePost = (id) => {
    for (let i = 0; i < posts.length; i++) {
        if (posts[i].id.toString() === id) {
            posts.splice(i, 1);
            showPosts(posts);
            alert('Successfully deleted!');
        }
    }
};